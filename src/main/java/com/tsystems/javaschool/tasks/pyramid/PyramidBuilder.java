package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> input) throws CannotBuildPyramidException{
        int N = input.size();
        if (!((Math.sqrt(8*N+1))-((int)(Math.sqrt(8*N+1))) == 0)) {
            throw  new CannotBuildPyramidException("Math error");
        }
        if(input.contains(null)){
            throw new CannotBuildPyramidException("Null error");
        }

        List<Integer> sort = input.stream().sorted().collect(Collectors.toList());
        int count = 0;
        int row = 0;
        while(count<N) {
            row++;
            count+=row;
        }
        int column = 2*row-1;
        int [][] output = new int[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                output[i][j] = 0;
            }
        }
        int center = (column-1)/2;
        int num = 0;
        for (int i = 0; i < row; i++) {
            for (int j = center - i; j < column - (center - i); j += 2) {
                output[i][j] = sort.get(num);
                num++;
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++){
                System.out.print(output[i][j]+" ");
            }
            System.out.println();
        }

        return output;
    }

    public static void main(String[] args){
        try {

            List<Integer> input = Arrays.asList(1, 1, 2, 2, 3);
            new PyramidBuilder().buildPyramid(input);
        }catch (CannotBuildPyramidException e){
            e.printStackTrace();
        }

    }
}
