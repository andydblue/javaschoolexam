package com.tsystems.javaschool.tasks.calculator;

import java.util.Iterator;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String in){
        Double res = null;
        String resStr = null;
        try {
            if(in.contains("--")||in.contains("++")||in.contains("**")||in.contains("//"))return null;
            res = calckRPN(getRPN(in), 4);
            resStr=res.toString();
        }catch (Exception ignore){}
        if(res!=null){
            if((res%(int)(double)res)==0){
                resStr=""+((int)(double)res);
            }
        }
        return resStr;
    }
    private int getPr(char i){
        if(i=='*'||i=='/') return 3;
        if(i=='+'||i=='-') return 2;
        if(i=='(') return 1;
        if(i==')') return -1;
        char [] chars =  {'0', '1','2','3','4','5','6','7','8','9','.'};
        for (char c : chars){
            if(c==i){return 0;}
        }
        return -2;
    }
    public String getRPN(String in) throws ArithmeticException {
        StringBuilder current = new StringBuilder("");
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < in.length(); i++) {
            char tempChar = in.charAt(i);
            int priority = getPr(tempChar);
            if(priority==-2) {
                throw new ArithmeticException();
            }
            if(priority==0){
                current.append(tempChar);
                if(i+1<in.length()&&getPr(in.charAt(i+1))!=0){
                    current.append(" ");
                }else if(i+1==in.length()){
                    current.append(" ");
                }
            }
            if(priority==1){
                stack.push(tempChar);
            }
            if(priority>1){
                if(stack.isEmpty()) {
                    stack.push(tempChar);
                }else {
                    while (!stack.isEmpty()&&priority<=getPr(stack.peek())){
                        current.append(stack.pop());
                    }
                    stack.push(tempChar);
                }
            }
            if(priority==-1){
                while (!(stack.peek()=='(')){
                    current.append(stack.pop());
                }
                stack.pop();
            }
        }
        Iterator<Character> it =  stack.iterator();
        while (it.hasNext()){
            char temp = it.next();
            if('('==temp){
                throw new ArithmeticException();
            }
        }
        while (!(stack.isEmpty())){
            current.append(stack.pop());
        }
        return current.toString();
    }
    private double calckRPN(String in , int c){
        StringBuffer op1=new StringBuffer();
        Stack<Double> stack = new Stack<>();
        for (int i = 0; i < in.length(); i++) {
            char tempChar = in.charAt(i);
            if(getPr(tempChar)==0){
                op1.append(tempChar);
                continue;
            }
            if(tempChar==' '&&(!op1.toString().equals(""))){
                stack.push(Double.parseDouble(op1.toString()));
                op1=new StringBuffer("");
                continue;
            }
            if(tempChar=='*'){
                double oper1 = stack.pop();
                double oper2 = stack.pop();
                stack.push(oper1*oper2);
            }
            if(tempChar=='/'){
                double oper1 = stack.pop();
                if(Double.compare(oper1, 0.0)==0) {
                    throw new ArithmeticException();
                }
                double oper2 = stack.pop();
                stack.push(oper2/oper1);
            }
            if(tempChar=='+'){
                double oper1 = stack.pop();
                double oper2 = stack.pop();
                stack.push(oper1+oper2);
            }
            if(tempChar=='-'){
                if(stack.size()==1){
                    double tempDouble = stack.pop();
                    stack.push(tempDouble*-1);
                }else {
                    double oper1 = stack.pop();
                    double oper2 = stack.pop();
                    stack.push(oper2 - oper1);
                }
            }
        }
        double result = stack.pop();
        result= result*Math.pow(10, c);
        result = Math.round(result);
        result=result/Math.pow(10, c);
        return result;
    }
    public static void main(String[] args) {
        Calculator c = new Calculator();
        System.out.println(c.evaluate("6+7*12-(11+10)*2"));
    }
}
