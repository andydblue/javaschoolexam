package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public <T> boolean find(List<T> list1, List<T> list2) throws IllegalArgumentException {
        if(list1==null||list2==null) throw  new IllegalArgumentException();

        try {
            int b = 0;
            int count = 0;

            for (int a = 0; a < list1.size(); a++) {
                T tX = list1.get(a);

                for (; b < list2.size(); b++) {
                    T tY = list2.get(b);
                    if (tX.equals(tY)) {
                        count++;
                        break;
                    }
                }

            }
            return count == list1.size();
        } catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }
    }
    public static void main(String[] args) throws IllegalArgumentException{
        Subsequence sub = new Subsequence();
        List<String> in1 = Arrays.asList("A","B","C","D");
        List<String> in2 = Arrays.asList("BD","A","ABC","B","M","D","M","C","DC");
        System.out.println(sub.find(in1, in2));
    }
}
